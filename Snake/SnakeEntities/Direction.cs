﻿namespace SnakeGame.SnakeEntities
{
    public enum Direction
    {
        Up,
        Right,
        Down,
        Left
    }
}
