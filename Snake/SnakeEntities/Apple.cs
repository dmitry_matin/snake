﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SnakeGame.SnakeEntities
{
    public class Apple : GameUnit
    {
        public int Value { get; set; } = 10;

        public bool IsEaten { get; set; } = false;

        public Apple(Point location) : base(location)
        {
            Shape = new Rectangle
            {
                Stroke = new SolidColorBrush(Colors.Transparent),
                Fill = new SolidColorBrush(Colors.Red),
                Width = StandardSize,
                Height = StandardSize
            };
        }             
    }
}
