﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SnakeGame.SnakeEntities
{
    public class SnakePart : GameUnit
    {    
        public SnakePart(Point location) : base(location)
        {
            Shape = new Rectangle
            {
                Stroke = new SolidColorBrush(Colors.Black),
                Fill = new SolidColorBrush(Colors.YellowGreen),
                Width = StandardSize,
                Height = StandardSize
            };
        }
    }
}
