﻿using System.Collections.Generic;
using System.Windows;

namespace SnakeGame.SnakeEntities
{
    public class Snake
    {
        private int startLength = 3;
        
        public List<SnakePart> Body { get; } = new List<SnakePart>();

        private Direction _direction = Direction.Right;

        public Direction Direction
        {
            get { return _direction; }
            set
            {
                if (!DirectionChangeInvalid(_direction, value)) _direction = value;
            }           
        }

        public SnakePart Head { get; set; }

        public int Speed { get; set; } = 240;

        public bool IsIntersectingItselft = false;

        public int Score { get; set; } = 0;

        public Snake()
        {
            for (int i = startLength - 1; i >= 0; i--)
            {
                Body.Add(new SnakePart(new Point(i * GameUnit.StandardSize,
                                                     GameUnit.StandardSize)));
            }

            Head = Body[0];
        }

        public void Move(Size gamingArea)
        {
            switch (_direction)
            {
                case Direction.Up:
                    MoveBody(0, -GameUnit.StandardSize);
                    break;
                case Direction.Right:
                    MoveBody(GameUnit.StandardSize, 0);
                    break;
                case Direction.Down:
                    MoveBody(0, GameUnit.StandardSize);
                    break;
                case Direction.Left:
                    MoveBody(-GameUnit.StandardSize, 0);
                    break;
            }

            ChangeSnakeLocation(gamingArea);
        }

        private void MoveBody(double x, double y)
        {
            for (int i = Body.Count - 1; i > 0; i--)
            {
                Body[i].Location = Body[i - 1].Location;
            }

            Head.Location = new Point(Head.Location.X + x,
                                      Head.Location.Y + y);

            IsIntersectingItselft = CheckSelfIntersection();
        }
        public void Accelerate()
        {
            if (Score % 40 == 0)
            {
                if (Speed > 160)
                    Speed -= 40;
                else if (Speed > 80)
                    Speed -= 20;
                else if (Speed > 20)
                    Speed -= 5;
            }
        }

        private bool CheckSelfIntersection()
        {
            for (int i = 1; i < Body.Count; i++)
            {
                if (Head.IsIntersectingGameUnit(Body[i]))
                    return true;
            }
            return false;
        }

        private void ChangeSnakeLocation(Size gamingArea)
        {
            if (Head.Location.X < 0)
                Head.Location = new Point(gamingArea.Width - GameUnit.StandardSize, Head.Location.Y);
            else if (Head.Location.X == gamingArea.Width)
                Head.Location = new Point(0, Head.Location.Y);

            else if (Head.Location.Y < 0)
                Head.Location = new Point(Head.Location.X, gamingArea.Height - GameUnit.StandardSize);
            else if (Head.Location.Y == gamingArea.Height)
                Head.Location = new Point(Head.Location.X, 0);
            else return;
        }

        private bool DirectionChangeInvalid(Direction currentDirection, Direction newDirection) =>
            (((int)currentDirection % 2 == 0 &&  (int)newDirection % 2 == 0) || 
            ((int)currentDirection % 2 != 0 && (int)newDirection % 2 != 0));

        public void EatApple(Apple apple)
        {
            Score += apple.Value;
            AppendTrail();
            apple.IsEaten = true;
        }

        private void AppendTrail()
        {
            SnakePart last = Body[Body.Count - 1];
            Body.Add(new SnakePart(new Point(last.Location.X, last.Location.Y)));
        }
    }
}