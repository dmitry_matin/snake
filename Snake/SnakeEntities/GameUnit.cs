﻿using System.Windows;
using System.Windows.Shapes;

namespace SnakeGame.SnakeEntities
{
    public abstract class GameUnit
    {
        public Point Location { get; set; }

        public static double StandardSize { get; set; } = 40;

        public Shape Shape { get; set; }

        public GameUnit(Point location)
        {
            Location = location;
        }

        public bool IsIntersectingGameUnit(GameUnit unit) =>
            Location.X == unit.Location.X && 
            Location.Y == unit.Location.Y;    
    }
}
