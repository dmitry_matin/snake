﻿using System;
using System.Collections.Generic;
using System.Windows.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SnakeGame.SnakeEntities;

namespace SnakeGame
{
    public partial class MainWindow : Window
    {
        private Size GamingArea => new Size(canvasPlayArea.ActualWidth, canvasPlayArea.ActualHeight);
        private Snake snake;
        private Apple apple;
        private DispatcherTimer Timer = new DispatcherTimer(DispatcherPriority.ApplicationIdle);

        private bool allowKeyPress = true;
        private bool fullScreen = false;
        private bool gameIsOn = false;
        private bool gameIsOver = false;
        private double currentFooterHeight;

        private List<Point> GridPoints;

        public MainWindow()
        {
            InitializeComponent();
            Timer.Tick += Timer_Elapsed;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            InitialiseStartState();
        }

        private void InitialiseStartState()
        {
            snake = new Snake();
            Timer.Interval = new TimeSpan(0, 0, 0, 0, snake.Speed);

            gameInfoLabel.Content = "Старт: любая клавиша\nУправление: стрелки, WASD";

            InitialiseGridPoints();
            DisplaySnake();
            AddApple();
        }

        private void InitialiseGridPoints()
        {
            GridPoints = new List<Point>((int)(GameUnit.StandardSize * GameUnit.StandardSize));

            for (int j = 0; j < GamingArea.Width; j += (int)GameUnit.StandardSize)
            {
                for (int k = 0; k < GamingArea.Height; k += (int)GameUnit.StandardSize)
                {
                    GridPoints.Add(new Point(j, k));
                }
            }
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            if (gameIsOn)
                return;
            StartGame();
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            StopGame();
        }

        private void StartGame()
        {
            if (gameIsOver)
            {
                RemoveSnake();
                RemoveApple();
                InitialiseStartState();
                gameIsOver = false;
            }
            gameIsOn = true;
            Timer.Start();
        }

        private void StopGame()
        {
            Timer.Stop();
            if (gameIsOver)
            {
                gameInfoLabel.Content = "Вы проиграли!";
            }
            gameIsOn = false;
        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (allowKeyPress == false || gameIsOver == true)
                return;

            if (gameIsOn == false)
                StartGame();

            if (e.Key == Key.Up || e.Key == Key.W)
            {
                snake.Direction = Direction.Up;
            }
            else if (e.Key == Key.Down || e.Key == Key.S)
            {
                snake.Direction = Direction.Down;
            }
            else if (e.Key == Key.Left || e.Key == Key.A)
            {
                snake.Direction = Direction.Left;
            }
            else if (e.Key == Key.Right || e.Key == Key.D)
            {
                snake.Direction = Direction.Right;
            }

            //full screen mode
            else if (e.Key == Key.F11)
            {
                if (fullScreen == false)
                {
                    WindowState = WindowState.Maximized;
                    WindowStyle = WindowStyle.None;

                    ChangeCanvasMargin();
                    fullScreen = true;
                }
                else if (fullScreen == true)
                {
                    WindowState = WindowState.Normal;
                    WindowStyle = WindowStyle.SingleBorderWindow;
                    ChangeCanvasMargin();
                    fullScreen = false;
                }
            }

            else return;
            allowKeyPress = false;
        }

        private void ChangeCanvasMargin()
        {
            if (fullScreen == false)
            {
                currentFooterHeight = footer.Height;

                double horizDiff, vertDiff;
                horizDiff = (GamingArea.Width - ((int)(GamingArea.Width / GameUnit.StandardSize) * GameUnit.StandardSize)) / 2;
                vertDiff = (GamingArea.Height - ((int)(GamingArea.Height / GameUnit.StandardSize) * GameUnit.StandardSize));

                footer.Height += vertDiff;
                canvasPlayArea.Margin = new Thickness(horizDiff, 0, horizDiff, footer.Height);
            }
            else if (fullScreen == true)
            {
                footer.Height = currentFooterHeight;
                canvasPlayArea.Margin = new Thickness(0, 0, 0, footer.Height);
            }
        }

        private void Timer_Elapsed(object sender, EventArgs e)
        {
            allowKeyPress = true;

            snake.Move(GamingArea);

            DisplaySnake();
            DisplayProgress();

            if (snake.Head.IsIntersectingGameUnit(apple))
            {
                snake.EatApple(apple);
                AddApple();

                snake.Accelerate();
                Timer.Interval = new TimeSpan(0, 0, 0, 0, snake.Speed);

            }

            if (snake.IsIntersectingItselft)
            {
                gameIsOver = true;
                StopGame();
            }
        }

        private void DisplaySnake()
        {
            RemoveSnake();

            foreach (var item in snake.Body)
            {
                Canvas.SetTop(item.Shape, item.Location.Y);
                Canvas.SetLeft(item.Shape, item.Location.X);
                canvasPlayArea.Children.Add(item.Shape);
            }
        }

        private void RemoveSnake()
        {
            foreach (var item in snake.Body)
            {
                canvasPlayArea.Children.Remove(item.Shape);
            }
        }

        private void AddApple()
        {
            //если яблоко уже есть и оно съедено, удаляем его с дисплея и создаем новое
            if (apple != null && apple.IsEaten)
                RemoveApple();

            Random ran = new Random();
            double x, y;
            bool intersectsWithSnakeBody = false;

            do
            {
                x = GridPoints[ran.Next(0, GridPoints.Count)].X;
                y = GridPoints[ran.Next(0, GridPoints.Count)].Y;

                foreach (var snakeBody in snake.Body)
                {
                    if (snakeBody.IsIntersectingGameUnit(new Apple(new Point(x, y))))
                    {
                        intersectsWithSnakeBody = true;
                        break;
                    }
                    else
                        intersectsWithSnakeBody = false;
                }
            }
            while (intersectsWithSnakeBody);

            apple = new Apple(new Point(x, y));

            DisplayApple();
        }

        private void DisplayApple()
        {
            Canvas.SetTop(apple.Shape, apple.Location.Y);
            Canvas.SetLeft(apple.Shape, apple.Location.X);
            canvasPlayArea.Children.Add(apple.Shape);
        }

        private void RemoveApple()
        {
            if (apple != null)
                canvasPlayArea.Children.Remove(apple.Shape);
        }

        private void DisplayProgress()
        {
            scoreLabel.Content = $"Очки: {snake.Score.ToString()}";
            snakeLengthLabel.Content = $"Длина змейки: {snake.Body.Count}";
        }

    }
}
